from flask import Flask, jsonify, request
import psycopg2
from psycopg2.extras import RealDictCursor
import hashlib

app = Flask(__name__)

# Database connection parameters
db_params = {
    'dbname': 'postgres',
    'user': 'postgres',
    'password': 'vericu',
    'host': '0.0.0.0',
    'port': '5432'
}

@app.route('/login', methods=['POST'])
def handle_login():
    table = 'public.users'
    username = request.form.get('uxp')
    password = request.form.get('pxp')
    hpass = sha256(password)

    try:
        # Connect to the PostgreSQL database
        conn = psycopg2.connect(**db_params)
        cursor = conn.cursor(cursor_factory=RealDictCursor)

        # Execute a query
        cursor.execute(f'SELECT * FROM {table} WHERE username=\'{username}\' AND pass=\'{hpass}\'')
        data = cursor.fetchall()

        # Close the cursor and connection
        cursor.close()
        conn.close()
        
        return jsonify(data)
    except Exception as e:
        print(f"Error: {e}")
        return jsonify({"error": str(e)}), 500

@app.route('/register', methods=['POST'])
def handle_register():
    table = 'public.users'
    email    = request.form.get('exp')
    username = request.form.get('uxp')
    password = request.form.get('pxp')
    hpass = sha256(password)

    try:
        # Connect to the PostgreSQL database
        conn = psycopg2.connect(**db_params)
        cursor = conn.cursor(cursor_factory=RealDictCursor)

        # Execute a query
        cursor.execute(f'INSERT INTO {table} (username,email,utype,pass) VALUES (\'{username}\',\'{email}\',\'BASE\',\'{hpass}\')')
        conn.commit()

        # Close the cursor and connection
        cursor.close()
        conn.close()
        
        return jsonify({"status": "success"}), 200
    except Exception as e:
        print(f"Error: {e}")
        return jsonify({"error": str(e)}), 500
    
@app.route('/additem', methods=['POST'])
def handle_additem():
    table = 'public.items'
    description = request.form.get('des')
    owner       = request.form.get('own')

    try:
        # Connect to the PostgreSQL database
        conn = psycopg2.connect(**db_params)
        cursor = conn.cursor(cursor_factory=RealDictCursor)

        # Execute a query
        cursor.execute(f'INSERT INTO {table} (description,owner) VALUES (\'{description}\',\'{owner}\')')
        conn.commit()

        # Close the cursor and connection
        cursor.close()
        conn.close()
        
        return jsonify({"status": "success"}), 200
    except Exception as e:
        print(f"Error: {e}")
        return jsonify({"error": str(e)}), 500
    
@app.route('/addissue', methods=['POST'])
def handle_addissue():
    table = 'public.issues'
    p_from =    request.form.get('from')
    p_to =      request.form.get('to')
    p_msg =     request.form.get('msg')

    try:
        # Connect to the PostgreSQL database
        conn = psycopg2.connect(**db_params)
        cursor = conn.cursor(cursor_factory=RealDictCursor)

        # Execute a query
        cursor.execute(f'INSERT INTO {table} (p_from,p_to,message) VALUES (\'{p_from}\',\'{p_to}\',\'{p_msg}\')')
        conn.commit()

        # Close the cursor and connection
        cursor.close()
        conn.close()
        
        return jsonify({"status": "success"}), 200
    except Exception as e:
        print(f"Error: {e}")
        return jsonify({"error": str(e)}), 500
    
@app.route('/viewissue', methods=['POST'])
def handle_viewissue():
    table = 'public.issues'
    p_to = request.form.get('to')

    try:
        # Connect to the PostgreSQL database
        conn = psycopg2.connect(**db_params)
        cursor = conn.cursor(cursor_factory=RealDictCursor)

        # Execute a query
        cursor.execute(f'SELECT * FROM {table} WHERE p_to=\'{p_to}\'')
        data = cursor.fetchall()

        # Close the cursor and connection
        cursor.close()
        conn.close()
        
        return jsonify(data)
    except Exception as e:
        print(f"Error: {e}")
        return jsonify({"error": str(e)}), 500
    
@app.route('/addreservation', methods=['POST'])
def handle_addreservation():
    table = 'public.reservation'
    reserver = request.form.get('res')
    locomotive = request.form.get('lid')
    timeslot = request.form.get('time')

    try:
        # Connect to the PostgreSQL database
        conn = psycopg2.connect(**db_params)
        cursor = conn.cursor(cursor_factory=RealDictCursor)

        # Execute a query
        cursor.execute(f'INSERT INTO {table} (reserver, locomotiveid, timeslot) VALUES (\'{reserver}\', \'{locomotive}\', \'{timeslot}\');')
        conn.commit()

        # Close the cursor and connection
        cursor.close()
        conn.close()
        
        return jsonify({"status": "success"}), 200
    except Exception as e:
        print(f"Error: {e}")
        return jsonify({"error": str(e)}), 500
    
def sha256(input_string):
    # Convert the string to bytes
    byte_string = input_string.encode('utf-8')
    
    # Create a SHA-256 hash object
    sha256_hash = hashlib.sha256()
    
    # Update the hash object with the byte-encoded string
    sha256_hash.update(byte_string)
    
    # Retrieve the hexadecimal representation of the hash
    hex_digest = sha256_hash.hexdigest()
    
    return hex_digest

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
