package com.example.locomotives.db;

import com.example.locomotives.data.IssueDataModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ViewIssueApiService {
    @FormUrlEncoded
    @POST("viewissue")
    Call<List<IssueDataModel>> queryIssues(
            @Field("to")    String to
    );
}
