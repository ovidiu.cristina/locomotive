package com.example.locomotives.db;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface AddReservationApiService {
    @FormUrlEncoded
    @POST("addreservation")
    Call<Void> insertReservation(
            @Field("res")  String reserver,
            @Field("lid")  String locomotive,
            @Field("time") String timeslot
    );
}
