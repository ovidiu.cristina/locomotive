package com.example.locomotives.db;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface UserRegisterApiService {
    @FormUrlEncoded
    @POST("register")
    Call<Void> insertUsers(
            @Field("exp")  String email,
            @Field("uxp")  String username,
            @Field("pxp")  String pass
    );
}