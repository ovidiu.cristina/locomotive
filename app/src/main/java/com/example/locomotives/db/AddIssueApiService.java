package com.example.locomotives.db;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface AddIssueApiService {
    @FormUrlEncoded
    @POST("addissue")
    Call<Void> insertUsers(
            @Field("from")  String from,
            @Field("to")    String to,
            @Field("msg")   String message
    );
}
