package com.example.locomotives.db;

import com.example.locomotives.data.UserDataModel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

import java.util.List;

public interface UserLoginApiService {
    @FormUrlEncoded
    @POST("login")
    Call<List<UserDataModel>> queryUsers(
            @Field("uxp")  String username,
            @Field("pxp")  String pass
    );
}