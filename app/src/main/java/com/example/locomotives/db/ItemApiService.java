package com.example.locomotives.db;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ItemApiService {
    @FormUrlEncoded
    @POST("additem")
    Call<Void> insertUsers(
            @Field("des")  String description,
            @Field("own")  String owner
    );
}
