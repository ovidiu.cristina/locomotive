package com.example.locomotives.user;

import com.example.locomotives.data.UserDataModel;

public class User {
    public static UserDataModel user;

    public static UserDataModel getUser() {
        return user;
    }

    public static void setUser(UserDataModel user) {
        User.user = user;
    }
}
