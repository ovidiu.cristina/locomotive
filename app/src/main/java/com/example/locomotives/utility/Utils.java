package com.example.locomotives.utility;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class Utils {
    public static void goToActivity(Context ctx,Class act){
        Intent intent = new Intent(ctx, act);
        ctx.startActivity(intent);
    }

    public static void message(Context ctx, String m){
        Toast.makeText(ctx,m,Toast.LENGTH_SHORT).show();
    }
}
