package com.example.locomotives.data;

public class IssueDataModel {
    private int id;
    private String p_from;
    private String p_to;
    private String message;

    public IssueDataModel(int id, String p_from, String p_to, String message) {
        this.id = id;
        this.p_from = p_from;
        this.p_to = p_to;
        this.message = message;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getP_from() {
        return p_from;
    }

    public void setP_from(String p_from) {
        this.p_from = p_from;
    }

    public String getP_to() {
        return p_to;
    }

    public void setP_to(String p_to) {
        this.p_to = p_to;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "IssueDataModel{" +
                "id=" + id +
                ", p_from='" + p_from + '\'' +
                ", p_to='" + p_to + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
