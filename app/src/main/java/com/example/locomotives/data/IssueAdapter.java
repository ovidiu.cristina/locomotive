package com.example.locomotives.data;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.locomotives.R;
import java.util.List;

public class IssueAdapter extends RecyclerView.Adapter<IssueAdapter.IssueViewHolder> {
    private List<IssueDataModel> issueList;

    public IssueAdapter(List<IssueDataModel> issueList) {
        this.issueList = issueList;
    }

    @NonNull
    @Override
    public IssueViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.issue_item, parent, false);
        return new IssueViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull IssueViewHolder holder, int position) {
        IssueDataModel issue = issueList.get(position);
        holder.idTextView.setText("ID: " + String.valueOf(issue.getId()));
        holder.fromTextView.setText("From: " + issue.getP_from());
        holder.toTextView.setText("To: " + issue.getP_to());
        holder.messageTextView.setText("Message: " + issue.getMessage());
    }

    @Override
    public int getItemCount() {
        return issueList.size();
    }

    public static class IssueViewHolder extends RecyclerView.ViewHolder {
        public TextView idTextView;
        public TextView fromTextView;
        public TextView toTextView;
        public TextView messageTextView;

        public IssueViewHolder(View itemView) {
            super(itemView);
            idTextView = itemView.findViewById(R.id.idTextView);
            fromTextView = itemView.findViewById(R.id.fromTextView);
            toTextView = itemView.findViewById(R.id.toTextView);
            messageTextView = itemView.findViewById(R.id.messageTextView);
        }
    }
}
