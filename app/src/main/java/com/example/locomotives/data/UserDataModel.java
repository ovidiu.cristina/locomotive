package com.example.locomotives.data;

public class UserDataModel {
    private int id;
    private String username;
    private String email;
    private String utype;
    private String pass;
    private String parent;

    public UserDataModel(int id, String username, String email, String utype, String pass,String parent) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.utype = utype;
        this.pass = pass;
        this.parent = parent;
    }

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getUtype() {
        return utype;
    }

    public String getPass() {
        return pass;
    }

    public String getParent() {
        return parent;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public void setUtype(String utype) {
        this.utype = utype;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    @Override
    public String toString() {
        return "UserDataModel{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", utype='" + utype + '\'' +
                ", pass='" + pass + '\'' +
                ", parent='" + parent + '\'' +
                '}';
    }
}
