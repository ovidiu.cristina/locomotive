package com.example.locomotives.pages.functions;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NavUtils;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.example.locomotives.R;
import com.example.locomotives.db.ApiClient;
import com.example.locomotives.db.ItemApiService;
import com.example.locomotives.db.UserLoginApiService;
import com.example.locomotives.db.UserRegisterApiService;
import com.example.locomotives.pages.Dashboard;
import com.example.locomotives.pages.credentials.Login;
import com.example.locomotives.user.User;
import com.example.locomotives.utility.Utils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddItem extends AppCompatActivity {
    private static final int MAX_RETRIES = 1;

    private ItemApiService apiService;

    private String description;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_add_items);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });

        //backed api
        apiService = ApiClient.getClient().create(ItemApiService.class);

        setBackBtn();

        setAddBtn();
    }

    private void setAddBtn(){
        Button button = findViewById(R.id.add_item_button);
        button.setOnClickListener(v -> {
            extractDescription();

            //make sql query
            queryAddItem(MAX_RETRIES);
        });
    }

    private void extractDescription(){
        TextView desc = findViewById(R.id.tag_state_description_item);

        description = desc.getText().toString();
    }

    private void setBackBtn(){
        // Find the toolbar in the layout
        Toolbar toolbar = findViewById(R.id.toolbar_item);

        // Set the Toolbar as the ActionBar
        setSupportActionBar(toolbar);

        // Get the ActionBar and set the Up button
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void queryAddItem(int retriesLeft) {
        Context ctx = this;

        Call<Void> call = apiService.insertUsers(description, User.getUser().getUsername());
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                // Handle the response data
                Log.d("API Response", "Success: Item Data inserted successfully");
                Utils.message(ctx,"Item created");

                //Goto dashboard
                Utils.goToActivity(ctx, Dashboard.class);
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                // Log the error
                t.printStackTrace();
                Log.e("API Error", t.getMessage());

                // Retry logic
                if (retriesLeft > 0) {
                    Log.d("API Response", "AddItem DB error: Retrying... (" + retriesLeft + " retries left)");
                    queryAddItem(retriesLeft - 1);
                } else {
                    Log.d("API Response", "Item cannot be added");
                }
            }
        });
    }
}