package com.example.locomotives.pages.credentials;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.example.locomotives.R;
import com.example.locomotives.data.UserDataModel;
import com.example.locomotives.db.ApiClient;
import com.example.locomotives.db.UserLoginApiService;
import com.example.locomotives.pages.Dashboard;
import com.example.locomotives.user.User;
import com.example.locomotives.utility.Utils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends AppCompatActivity {
    private static final int MAX_RETRIES = 5;

    private UserLoginApiService userApiService;

    protected String username;
    protected String pass;

    protected void extractCredentials() {
        EditText editTextUsername = findViewById(R.id.username);
        EditText editTextPass = findViewById(R.id.password);

        username = editTextUsername.getText().toString();
        pass = editTextPass.getText().toString();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_login);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });

        //backed api
        userApiService = ApiClient.getClient().create(UserLoginApiService.class);

        //login button handler
        setLoginBtn();

        //register link
        setRegLink();
    }

    private void queryLogin(int retriesLeft) {
        Context ctx = this;

        Call<List<UserDataModel>> call = userApiService.queryUsers(username, pass);
        call.enqueue(new Callback<List<UserDataModel>>() {
            @Override
            public void onResponse(Call<List<UserDataModel>> call, Response<List<UserDataModel>> response) {
                if (response.isSuccessful()) {
                    List<UserDataModel> data = response.body();
                    // Handle the response data
                    Log.d("API Response", "Success: " + data);
                    if(data.size() == 1){
                        //save user details
                        User.setUser(data.get(0));

                        //user exists
                        Utils.goToActivity(ctx, Dashboard.class);
                    }
                    else{
                        Utils.message(ctx,"Invalid credentials!");
                    }
                } else {
                    // Handle the error response
                    Log.d("API Response", "Failure: " + response.errorBody());
                    Utils.message(ctx,"DB error");
                }
            }

            @Override
            public void onFailure(Call<List<UserDataModel>> call, Throwable t) {
                // Log the error
                t.printStackTrace();
                Log.e("API Error", t.getMessage());

                // Retry logic
                if (retriesLeft > 0) {
                    Log.d("API Response", "Login DB error: Retrying... (" + retriesLeft + " retries left)");
                    queryLogin(retriesLeft - 1);
                } else {
                    Log.d("API Response", "Login DB failure.");
                }
            }
        });
    }

    private void setRegLink(){
        Context ctx = this;
        TextView reglink = findViewById(R.id.reglink);
        reglink.setOnClickListener(v -> {
            // No account ?
            // Redirect to Register page

            Utils.goToActivity(ctx,Register.class);
        });
    }

    private void setLoginBtn(){
        Button button = findViewById(R.id.login);
        button.setOnClickListener(v -> {
            extractCredentials();

            //handle sql query
            queryLogin(MAX_RETRIES);
        });
    }
}