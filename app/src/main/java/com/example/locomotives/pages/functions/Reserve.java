package com.example.locomotives.pages.functions;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NavUtils;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.example.locomotives.R;
import com.example.locomotives.db.AddIssueApiService;
import com.example.locomotives.db.AddReservationApiService;
import com.example.locomotives.db.ApiClient;
import com.example.locomotives.pages.Dashboard;
import com.example.locomotives.pages.credentials.Register;
import com.example.locomotives.user.User;
import com.example.locomotives.utility.Utils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Reserve extends AppCompatActivity {
    private static final int MAX_RETRIES = 1;

    private AddReservationApiService apiService;

    private int locomotive;
    private int timelot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_reserve_locomotive);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });

        //backed api
        apiService = ApiClient.getClient().create(AddReservationApiService.class);

        //back btn
        setBackBtn();

        //set reserve btn
        setReserveBtn();
    }

    private void setBackBtn(){
        // Find the toolbar in the layout
        Toolbar toolbar = findViewById(R.id.toolbar_reserve);

        // Set the Toolbar as the ActionBar
        setSupportActionBar(toolbar);

        // Get the ActionBar and set the Up button
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void setReserveBtn() {
        Button button = findViewById(R.id.reserve_locomotive);
        button.setOnClickListener(v -> {
            extractData();

            //handle sql query
            queryReserve(MAX_RETRIES);
        });
    }

    private void extractData(){
        EditText locomotiveE = findViewById(R.id.locomotive_id);
        EditText timeE = findViewById(R.id.time_slot);

        locomotive = Integer.valueOf(locomotiveE.getText().toString());
        timelot = Integer.valueOf(timeE.getText().toString());
    }

    private void queryReserve(int retriesLeft) {
        Context ctx = this;

        Call<Void> call = apiService.insertReservation(User.getUser().getUsername(),String.valueOf(locomotive),String.valueOf(timelot));
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                // Handle the response data
                Log.d("API Response", "Success: Reservation inserted successfully");
                Utils.message(ctx,"Reservation created");

                //Goto dashboard
                Utils.goToActivity(ctx, Dashboard.class);
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                // Log the error
                t.printStackTrace();
                Log.e("API Error", t.getMessage());

                // Retry logic
                if (retriesLeft > 0) {
                    Log.d("API Response", "Reserve DB error: Retrying... (" + retriesLeft + " retries left)");
                    queryReserve(retriesLeft - 1);
                } else {
                    Log.d("API Response", "Reservation cannot be added");
                }
            }
        });
    }
}