package com.example.locomotives.pages.functions;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.appcompat.widget.Toolbar;

import com.example.locomotives.R;
import com.example.locomotives.data.IssueAdapter;
import com.example.locomotives.data.IssueDataModel;
import com.example.locomotives.data.UserDataModel;
import com.example.locomotives.db.ApiClient;
import com.example.locomotives.db.ViewIssueApiService;
import com.example.locomotives.pages.Dashboard;
import com.example.locomotives.user.User;
import com.example.locomotives.utility.Utils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ViewIssue extends AppCompatActivity {
    private static final int MAX_RETRIES = 5;

    private ViewIssueApiService apiService;
    private RecyclerView recyclerView;
    private IssueAdapter issueAdapter;
    private List<IssueDataModel> issueList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_view_issue);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });

        //set recyclerView
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        //backed api
        apiService = ApiClient.getClient().create(ViewIssueApiService.class);

        setBackBtn();

        //view issues
        queryIssues(MAX_RETRIES);
    }

    private void setBackBtn(){
        // Find the toolbar in the layout
        Toolbar toolbar = findViewById(R.id.toolbar_view_issues);

        // Set the Toolbar as the ActionBar
        setSupportActionBar(toolbar);

        // Get the ActionBar and set the Up button
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void queryIssues(int retriesLeft) {
        Context ctx = this;

        Call<List<IssueDataModel>> call = apiService.queryIssues(User.getUser().getUsername());
        call.enqueue(new Callback<List<IssueDataModel>>() {
            @Override
            public void onResponse(Call<List<IssueDataModel>> call, Response<List<IssueDataModel>> response) {
                if (response.isSuccessful()) {
                    List<IssueDataModel> data = response.body();
                    // Handle the response data
                    Log.d("API Response", "Success: " + data);
                    if(!data.isEmpty()){
                        //save user details
                        issueList = data;

                        //display issues
                        dispIssues();
                    }
                    else{
                        Utils.message(ctx,"No issues");
                    }
                } else {
                    // Handle the error response
                    Log.d("API Response", "Failure: " + response.errorBody());
                    Utils.message(ctx,"Can't get issues");
                }
            }

            @Override
            public void onFailure(Call<List<IssueDataModel>> call, Throwable t) {
                // Log the error
                t.printStackTrace();
                Log.e("API Error", t.getMessage());

                // Retry logic
                if (retriesLeft > 0) {
                    Log.d("API Response", "View Issues DB error: Retrying... (" + retriesLeft + " retries left)");
                    queryIssues(retriesLeft - 1);
                } else {
                    Log.d("API Response", "Cannot get issues");
                }
            }
        });
    }

    private void dispIssues(){
        issueAdapter = new IssueAdapter(issueList);
        recyclerView.setAdapter(issueAdapter);
    }
}