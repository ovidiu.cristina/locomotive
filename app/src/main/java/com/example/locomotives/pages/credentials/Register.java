package com.example.locomotives.pages.credentials;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.example.locomotives.R;
import com.example.locomotives.db.ApiClient;
import com.example.locomotives.db.UserRegisterApiService;
import com.example.locomotives.utility.Utils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Register extends AppCompatActivity {
    private static final int MAX_RETRIES = 1;

    private UserRegisterApiService userApiService;

    protected String email;
    protected String username;
    protected String pass;

    protected void extractCredentials(){
        EditText editTextEmail = findViewById(R.id.email);
        EditText editTextUsername = findViewById(R.id.username);
        EditText editTextPass = findViewById(R.id.password);

        email = editTextEmail.getText().toString();
        username = editTextUsername.getText().toString();
        pass = editTextPass.getText().toString();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_register);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });

        //backed api
        userApiService = ApiClient.getClient().create(UserRegisterApiService.class);

        //set back button
        setBackBtn();

        //set register button
        setRegisterBtn();
    }

    private void setRegisterBtn(){
        Button button = findViewById(R.id.register); // Replace "button" with the ID of your Button
        button.setOnClickListener(v -> {
            extractCredentials();

            //make sql query
            queryRegister(MAX_RETRIES);
        });
    }

    private void queryRegister(int retriesLeft) {
        Context ctx = this;

        Call<Void> call = userApiService.insertUsers(email,username,pass);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                // Handle the response data
                Log.d("API Response", "Success: Data inserted successfully");

                //Goto dashboard
                Utils.goToActivity(ctx,Login.class);
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                // Log the error
                t.printStackTrace();
                Log.e("API Error", t.getMessage());

                // Retry logic
                if (retriesLeft > 0) {
                    Log.d("API Response", "Register DB error: Retrying... (" + retriesLeft + " retries left)");
                    queryRegister(retriesLeft - 1);
                } else {
                    Log.d("API Response", "User cannot be created");
                }
            }
        });
    }

    private void setBackBtn(){
        // Find the toolbar in the layout
        Toolbar toolbar = findViewById(R.id.toolbar_register);

        // Set the Toolbar as the ActionBar
        setSupportActionBar(toolbar);

        // Get the ActionBar and set the Up button
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }
}