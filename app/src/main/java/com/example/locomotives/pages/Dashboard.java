package com.example.locomotives.pages;

import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.activity.EdgeToEdge;
import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.example.locomotives.R;
import com.example.locomotives.pages.functions.AddIssue;
import com.example.locomotives.pages.functions.AddItem;
import com.example.locomotives.pages.functions.Reserve;
import com.example.locomotives.pages.functions.ViewIssue;
import com.example.locomotives.user.User;
import com.example.locomotives.utility.Utils;
import com.google.android.material.navigation.NavigationView;

public class Dashboard extends AppCompatActivity {

    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle toggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_dashboard);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });

        setMenu();

        setDashboardText();
    }

    private void setDashboardText() {
        TextView usernameV = findViewById(R.id.usernameV);
        TextView emailV = findViewById(R.id.emailV);
        TextView utypeV = findViewById(R.id.utypeV);
        TextView parentV = findViewById(R.id.parentV);

        usernameV.setText(User.getUser().getUsername());
        emailV.setText(User.getUser().getEmail());
        utypeV.setText(User.getUser().getUtype());
        parentV.setText(User.getUser().getParent());
    }

    private void setMenu(){
        drawerLayout = findViewById(R.id.main);
        NavigationView navigationView = findViewById(R.id.nav_view);

        //hide if not manager
        hideMenuItems(navigationView.getMenu());

        toggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                if(item.getItemId() == R.id.nav_add_item) {
                    Utils.goToActivity(Dashboard.this, AddItem.class);
                }
                else if(item.getItemId() == R.id.nav_add_issue) {
                    Utils.goToActivity(Dashboard.this, AddIssue.class);
                }
                else if(item.getItemId() == R.id.nav_view_issue) {
                    Utils.goToActivity(Dashboard.this, ViewIssue.class);
                }
                else if (item.getItemId() == R.id.nav_reserve) {
                    Utils.goToActivity(Dashboard.this, Reserve.class);
                }
                else {
                    return false;
                }
                drawerLayout.closeDrawers();
                return true;
            }
        });
    }

    private void hideMenuItems(Menu menu) {
        // Hide "View Issues" if user is not a MANAGER
        if (!User.getUser().getUtype().equals("MANAGER")) {
            menu.findItem(R.id.nav_view_issue).setVisible(false);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (toggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
